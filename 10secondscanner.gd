extends Node2D

var time = 0
onready var root = get_tree().get_root().get_node("Root")

func _process(delta):
	# set width of background bar
	var bg = self.get_node("background")
	var screenwidth = get_viewport().size.x
	bg.rect_size.x = screenwidth
	
	# set cursor position
	var time = root.time
	var proportion = time / 10
	$cursor.rect_position.x = proportion * screenwidth


func add_input(input):
	# parse the input
	var time = input[0]
	var type = input[1]
	var vec = input[2]
	
	# make an arrow sprite
	var sprite = Sprite.new()
	sprite.texture = load("res://arrow.svg")
	sprite.position.x = (time / 10) * $background.rect_size.x
#	sprite.position.y = 20
	sprite.position.y = $background.rect_position.y / 2
	sprite.scale.x = 0.4
	sprite.scale.y = 0.4
	sprite.rotation = vec.angle()
	if type == "shoot":
		sprite.modulate = Color("#ff0000")
	else:
		sprite.modulate = Color("#ffffff")
	# add to tree
	self.add_child(sprite)
