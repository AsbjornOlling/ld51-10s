extends Node2D

onready var root = get_tree().get_root().get_node("Root")
onready var player = root.get_node("Player")
export var projectile_interval = (10.0 / 2)
var projectile_countdown = 1.25
var projectile_scene = preload("res://Projectile.tscn")
var shot_scene = preload("res://Particles/Shot.tscn")
export var shoot_speed: float = 150


func close_enough_to(a: Vector2, b: Vector2):
	# utility function for snapping angles to closest cardinal
	return abs(a.angle_to(b)) < (PI/4)

func _process(delta):
	projectile_countdown -= delta
	if projectile_countdown < 0:
		# calculate shooting vector to player
		var vector_to_player: Vector2 = player.position - self.position
		var player_angle = vector_to_player.angle()
		
		# snap to closest cardinal direction
		var motion: Vector2
		if close_enough_to(vector_to_player, Vector2.RIGHT):
			motion = Vector2.RIGHT
		elif close_enough_to(vector_to_player, Vector2.LEFT):
			motion = Vector2.LEFT
		elif close_enough_to(vector_to_player, Vector2.UP):
			motion = Vector2.UP
		#elif close_enough_to(vector_to_player, Vector2.DOWN):
		else:
			motion = Vector2.DOWN
		motion *= shoot_speed

		# spawn projectile
		var projectile = projectile_scene.instance()
		projectile.init(self.position, motion, self)
		root.add_child(projectile)
		
		var shot_explosion = shot_scene.instance()
		shot_explosion.position = self.position
		shot_explosion.get_node("Explosion").direction = motion
		shot_explosion.get_node("Explosion").color = self.get_node("ColorRect").color
		root.add_child(shot_explosion)
		
		#print("shooting from " + str(self.position))
		
		# reset counter
		projectile_countdown = projectile_interval

func hit(by_player: bool):
	#print("enemy hit!")
	root.get_node("Camera2D").small_shake()
	if by_player:
		root.increment_score()
	self.queue_free()
	root.apply_clear_bonus(self)
