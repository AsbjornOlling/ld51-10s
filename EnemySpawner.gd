extends Node2D

var spawn_x
var spawn_y
onready var root = get_tree().get_root().get_node("Root")
var child

func enemy_alive():
	# check if self.child was freed
	var wr = weakref(self.child)
	if (!wr.get_ref()):
		# yep, it was freed
		return false
	else:
		return child != null

func new_enemy(x: int, y: int):
	if self.enemy_alive():
		print("Enemy already present. not spawning a new one.")
		return null
	var enemy = load("res://Enemy.tscn")
	child = enemy.instance()
	child.position.x = x
	child.position.y = y
	return child

func _ready():
	var new = new_enemy(spawn_x, spawn_y)
	if not new:
		return
	root.add_child(new)
	get_node("SpawnTimer").start()

func _on_SpawnTimer_timeout():
	var new = new_enemy(spawn_x, spawn_y)
	if not new:
		return
	new.get_node("ColorRect").color = "#660000"
	root.add_child(new)

func _on_InitTimer_timeout():
	pass
