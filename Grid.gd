extends Node2D


export var strokewidth = 5
export var color = Color("#1b1b1b") 
onready var root = get_tree().get_root().get_node("Root")

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(root, "ready")
	self.position.x = root.left_offset - (root.box_size / 2) - (strokewidth / 2)
	self.position.y = root.top_offset - (root.box_size / 2) - (strokewidth / 2)

	for x in range(0, root.grid_num + 1):
		var horizontal_line = ColorRect.new()
		horizontal_line.rect_size.x = strokewidth
		horizontal_line.rect_size.y = root.grid_size_int
		horizontal_line.rect_position.x = x * root.box_size
		horizontal_line.color = self.color
		self.add_child(horizontal_line)
	for y in range(0, root.grid_num + 1):
		var vertical_line = ColorRect.new()
		vertical_line.rect_size.x = root.grid_size_int
		vertical_line.rect_size.y = strokewidth
		vertical_line.rect_position.y = y * root.box_size
		vertical_line.color = self.color
		self.add_child(vertical_line)
