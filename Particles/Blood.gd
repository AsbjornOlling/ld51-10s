extends Node2D

onready var root = get_tree().get_root().get_node("Root")

var dir: Vector2
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$NarrowBlood.emitting = true
	$WideBlood.emitting = true
	$Body.emitting = true
	$Body.scale.x = root.get_node("Player").get_node("ColorRect").rect_size.x
	$Body.scale.y = root.get_node("Player").get_node("ColorRect").rect_size.y
	$NarrowBlood.direction = dir
	$WideBlood.direction = dir
	$Body.direction = dir
	
	pass # Replace with function body.

func _process(delta):
	pass
	#print(self.position)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	
	self.queue_free()
	
	pass # Replace with function body.
