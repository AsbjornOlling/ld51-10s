extends Node2D

var input_events = []
onready var root = get_tree().get_root().get_node("Root")
export var shoot_speed = 150
onready var scanner = self.get_node("../Scanner")
var projectile_scene = preload("res://Projectile.tscn")
var shot_scene = preload("res://Particles/Shot.tscn")
var trail_scene = preload("res://Particles/slime_trail.tscn")
var shoot_cooldown = 1
var shoot_countdown = 0

var projectiles = 1


var grid_coord = Vector2()
var left_offset = 0
var top_offset = 0
var grid_num = 0
var box_size = 0

var last_motion: Vector2

var blood_scene = preload("res://Particles/Blood.tscn")

func _ready():
	yield(root, "ready")
	
	self.get_node("Hitbox").connect("area_entered", self, "_on_area_entered")
	$StretchTween.connect("tween_completed", self, "_stretch_back")
	
	left_offset = root.left_offset
	top_offset = root.top_offset
	grid_num = root.grid_num
	box_size = root.box_size
	
	grid_coord.x = int(root.grid_num / 2)
	grid_coord.y = int(root.grid_num / 2)
	
	position.x = left_offset + box_size * grid_coord.x
	position.y = top_offset + box_size * grid_coord.y
	
	# move spawnbox to the right place and size
	var spawnbox = root.get_node("Spawnbox")
	var rect = spawnbox.get_node("ColorRect")
	rect.rect_size.x = box_size
	rect.rect_size.y = box_size
	spawnbox.offset.x = self.position.x - (box_size / 2)
	spawnbox.offset.y = self.position.y - (box_size / 2)
	

func spawn_projectile(shootvec):
	
	var projectile = projectile_scene.instance()
	projectile.init(self.position, shootvec, self)
	projectile.get_node("ColorRect").color = self.get_node("ColorRect").color
	root.add_child(projectile)
	

func force_into_grid(x):
	return min(max(0, x), root.grid_num - 1)
	

func _stretch_back(object, key):
	if self.scale.x > 1.1:
		print("going back")
		print(scale.length())
		$StretchTween.interpolate_property(
			self, "scale",
			self.scale, Vector2(1,1), 0.05,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN_OUT
		)
	

func _process(delta):
	var motion = motion_from_input()
	grid_coord.x += motion.x
	grid_coord.y += motion.y
	
	if motion != Vector2(0, 0):
		var trail = trail_scene.instance()
		trail.position = self.position
		trail.get_node("trail").color = self.get_node("ColorRect").color
		root.add_child(trail)
	
	grid_coord.x = force_into_grid(grid_coord.x)
	grid_coord.y = force_into_grid(grid_coord.y)
	
	var target_position = Vector2(
		grid_coord.x * box_size + left_offset,
		grid_coord.y * box_size + top_offset
	)
	
	
	# move smoothly
	if not $PositionTween.is_active() and target_position != self.position:
		$PositionTween.interpolate_property(
			self, "position",
			self.position, target_position, 0.05,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN_OUT
		)
		$PositionTween.start()
	
	# stretch animation
	if not $StretchTween.is_active() and target_position != self.position:
		# TODO: set rotation for tween stretch
		self.rotation = motion.angle()
		$StretchTween.interpolate_property(
			self, "scale",
			self.scale, Vector2(1.5,0.8), 0.05,
			Tween.TRANS_BACK,
			Tween.EASE_IN_OUT
		)
		$StretchTween.start()
		print("yep")

	var shootvec = shoot_vector()
	shoot_countdown -= delta
	if shoot_countdown < 0:
		shoot_countdown = 0
		if shootvec and projectiles > 0:
			projectiles -= 1
			get_node("AmmoIndicator").visible = true
			# spawn projectile
			spawn_projectile(shootvec)
			
			shoot_countdown = shoot_cooldown
			var shoot_input = [root.time, "shoot", shootvec]
			input_events.append(shoot_input)
			scanner.add_input(shoot_input)
	
	# find old inputs
	var recorded_inputs = get_inputs_for_now(delta)
	for event in recorded_inputs:
		if event[1] == "move":
			grid_coord += event[2]
			last_motion = event[2]
			var trail = trail_scene.instance()
			trail.position = self.position
			trail.get_node("trail").color = self.get_node("ColorRect").color
			root.add_child(trail)
			
		if event[1] == "shoot":
			spawn_projectile(event[2])
	
	# append this input
	if motion.length() != 0:
		var move_input = [root.time, "move", motion]
		input_events.append(move_input)
		# add graphics to top bar
		scanner.add_input(move_input)


func shoot_vector():
	var shootvec = Vector2()
	var key_pressed = false
	if Input.is_action_pressed("shoot_right"):
		shootvec.x += 1
		key_pressed = true
	if Input.is_action_pressed("shoot_left"):
		shootvec.x -= 1
		key_pressed = true
	if Input.is_action_pressed("shoot_down"):
		shootvec.y += 1
		key_pressed = true
	if Input.is_action_pressed("shoot_up"):
		shootvec.y -= 1
		key_pressed = true
	if key_pressed:
		shootvec = shootvec.normalized() * self.shoot_speed
		return shootvec
	else:
		return null


func motion_from_input():
	var motion = Vector2()
	if Input.is_action_just_pressed("move_right") and grid_coord.x < grid_num - 1:
		motion.x += 1
		last_motion.x = 1
		last_motion.y = 0
	elif Input.is_action_just_pressed("move_left") and grid_coord.x > 0:
		motion.x -= 1
		last_motion.x = -1
		last_motion.y = 0
	elif Input.is_action_just_pressed("move_down") and grid_coord.y < grid_num - 1:
		motion.y += 1
		last_motion.x = 0
		last_motion.y = 1
	elif Input.is_action_just_pressed("move_up") and grid_coord.y > 0:
		motion.y -= 1
		last_motion.x = 0
		last_motion.y = -1
	return motion


func get_inputs_for_now(delta):
	var now = self.root.time
	var results = []
	var search_intervals = []
	
	# generate up to two intervals, representing the time since last frame
	search_intervals.append([max(0, now - delta), now])
	if delta > now:
		search_intervals.append([10 - delta, 10])
	
	for event in self.input_events:
		var t = event[0]
		
		for interval in search_intervals:
			var n1 = interval[0]
			var n2 = interval[1]
			
			# check whether the event happened inside the interval
			if n1 < t and t < n2:
				results.append(event)
	return results

func _on_area_entered(area):
	print("entered area")
	print(area)
	if area.has_method("hit"):
		print("area has 'hit'")
		print(last_motion)
		# if collider has hit
		var blood = blood_scene.instance() 
		blood.position = area.position
		blood.dir = last_motion
		root.add_child(blood)
		area.hit(true)
	elif area.get_node("..").has_method("hit"):
		print("area has 'hit'")
		print(last_motion)
		# if collider has hit
		var blood = blood_scene.instance() 
		blood.position = area.get_node("..").position
		blood.dir = last_motion
		root.add_child(blood)
		area.get_node("..").hit(true)

func hit(by_player: bool):
	print("player hit")
	self.get_tree().change_scene("res://Retry.tscn")
