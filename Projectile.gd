extends Node2D

var target: Node
var motion: Vector2
var shooter: Node2D

var blood_scene = preload("res://Particles/Blood.tscn")
var miss_penalty_scene = preload("res://MissPenalty.tscn")
var shot_scene = preload("res://Particles/Shot.tscn")


onready var root = get_tree().get_root().get_node("Root")

func init(pos: Vector2, motion: Vector2, shooter: Node2D):
	self.position = pos
	self.target = target
	self.motion = motion
	self.shooter = shooter
	self.rotate(motion.angle())
	self.get_node("Hitbox").connect("area_entered", self, "_on_area_entered")



func check_bounds():
	var out_of_bounds = false
	if (self.position.x + root.box_size/2) < root.left_offset or root.left_offset + root.grid_size_int <= (self.position.x + root.box_size / 2):
		out_of_bounds = true
	if (self.position.y + root.box_size/2) < root.top_offset or root.top_offset + root.grid_size_int <= (self.position.y + root.box_size / 2):
		out_of_bounds = true
	
	if out_of_bounds:
		
		var shot_explosion = shot_scene.instance()
		shot_explosion.position = self.position
		shot_explosion.get_node("Explosion").direction = -1 * self.motion
		shot_explosion.get_node("Explosion").color = self.get_node("ColorRect").color
		root.add_child(shot_explosion)
		
		if self.was_shot_by_player():
			# trigger miss penalty
			print("adding penalty")
			var penalty = miss_penalty_scene.instance()
			penalty.position = self.position
			root.add_child(penalty)
			penalty.get_node("AnimationPlayer").play("animation")
			
			# subtract 1 point
			root.increment_score(-1)

		self.queue_free()

func was_shot_by_player():
	# check if shooter node was freed
	# (needed to avoid crash)
	var wr = weakref(self.shooter)
	if (!wr.get_ref()):
		# freed
		return false
	else:
		print(self.shooter.name)
		# not freed, check name
		return self.shooter.name == "Player"

func _process(delta):
	if not motion:
		return
	self.translate(motion * delta)
	self.check_bounds()

func _on_area_entered(area):
	if area == self.shooter or area.get_node("..") == self.shooter:
		#print("projectile hit its shooter")
		return
		
		
	if area.has_method("hit"):
		# if collider has hit
		var blood = blood_scene.instance()
		blood.position = area.position
		blood.dir = motion
		root.add_child(blood)
		area.hit(self.was_shot_by_player())
		self.queue_free()
	elif area.get_node("..").has_method("hit"):
		
		var blood = blood_scene.instance()
		blood.position = area.get_node("..").position
		blood.dir = motion
		root.add_child(blood)
				# if collider has hit
		area.get_node("..").hit(self.was_shot_by_player())
		self.queue_free()
	else:
		pass#print("projectile hit body with no hit() method.")
