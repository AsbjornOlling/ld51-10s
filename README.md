![timeslime banner image](./banner.png)


# [🕹️ Click here to play the game.](https://asbjornolling.gitlab.io/timeslime/)

Timeslime is a game we made for Ludum Dare 51.

It's a 2D shooter on a grid with a twist: Your moves are being recorded, and repeated every ten seconds.

Be careful and strategic with your inputs, and you might stand a chance.

The game is made using Godot, Aseprite and a Pocket Operator.

Move around using WASD, shoot using arrow keys.

![screenshot](./screenshot.png)
