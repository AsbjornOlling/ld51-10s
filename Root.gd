extends Node

var time = 0
var grid_num = 11

onready var width = get_viewport().size.x
onready var height = get_viewport().size.y 
onready var smallest = min(width, height)
onready var grid_size_float = smallest * 0.7
onready var grid_size_int = grid_num * int(grid_size_float/grid_num)
onready var box_size = grid_size_int / grid_num
onready var left_offset = (width - grid_size_int) / 2 + box_size / 2
onready var top_offset = (height - grid_size_int) / 2 + box_size / 2

onready var player = get_node("Player")

var score = 0
onready var highscore = load_highscore()

func increment_score(amount=1):
	self.score += amount
	$Score.text = str(self.score).pad_zeros(2)
	if score > highscore:
		highscore = score
		save_highscore()

func save_highscore():
	# save highscore
	if not OS.is_userfs_persistent():
		print("persistence not enabled. skipping save.")
		return
	var file = File.new()
	file.open("user://highscore", File.WRITE)
	file.store_string(str(highscore))
	$Highscore/Label.text = "BEST\n" + str(highscore)
	print("saving highscore: " + str(highscore))


func load_highscore():
	if not OS.is_userfs_persistent():
		print("persistence not enabled. skipping load.")
		return 0
	var file = File.new()
	file.open("user://highscore", File.READ)
	var highscore_str = file.get_as_text()
	$Highscore/Label.text = "BEST\n" + highscore_str
	return int(highscore_str)


var spawn_x = int(grid_num / 2)
var spawn_y = int(grid_num / 2)

func new_enemy_spawner(x, y):
	var enemy_spawner = load("res://EnemySpawner.tscn")
	var new = enemy_spawner.instance()
	new.spawn_x = x
	new.spawn_y = y
	return new

func add_new_enemy_spawner(player_x, player_y, is_first):
	randomize()
	var axis = randi() % 2
	var pos_x
	var pos_y
	if is_first:
		#print("first")
		if axis == 0:
			var guess = player_x
			while abs(guess - player_x) < 2:
				guess = randi() % grid_num
			pos_x = guess * box_size + left_offset
			pos_y = player_y * box_size + top_offset
		if axis == 1:
			var guess = player_y
			while abs(guess - player_y) < 2:
				guess = randi() % grid_num
			pos_x = player_x * box_size + left_offset
			pos_y = guess * box_size + top_offset
	else:
		var enemy_positions = []
		for node in self.get_children():
			if node.name == "EnemySpawner" or node.name.begins_with("@EnemySpawner@"):
				print(node)
				enemy_positions.append(Vector2(node.spawn_x, node.spawn_y))
		#if grid is full except spaces around player
		if len(enemy_positions) > (grid_num * grid_num) - 9:
			print("full!")
		else:
			#keep generating random spots on grid until it hits an empty one.
			var random_x
			var random_y
			random_x = randi() % grid_num
			random_y = randi() % grid_num
			print(enemy_positions)
			while abs(random_x - player_x) < 2 and abs(random_y - player_y) < 2 and not Vector2(random_x, random_y) in enemy_positions:
				random_x = randi() % grid_num
				random_y = randi() % grid_num
			pos_x = random_x * box_size + left_offset
			pos_y = random_y * box_size + top_offset
	add_child(new_enemy_spawner(pos_x,pos_y))
	

func apply_clear_bonus(skip_node: Node):
	# apply a 10 point bonus if all enemies are dead
	# look for enemies
	for node in self.get_children():
		if node == skip_node:
			continue
		if node.name == "Enemy" or node.name.begins_with("@Enemy@"):
			# found an enemy, not clear!
			print("found enemy, not clear")
			return
	# No enemies found, reward!
	$ClearBonus/AnimationPlayer.play("animation")
	self.increment_score(10)

func _ready():
	# initalize randomness
	
	randomize()
	add_new_enemy_spawner(spawn_x, spawn_y, true)
	
# called on every frame
func _process(delta):
	time += delta
	if time > 10:
		time -= 10
		spawn_x = player.grid_coord.x
		spawn_y = player.grid_coord.y
		player.projectiles = 1
		player.get_node("AmmoIndicator").visible = false
		add_new_enemy_spawner(spawn_x, spawn_y, false)

